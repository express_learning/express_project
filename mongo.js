const mongoose = require("mongoose")

//connecting mongoDb to nodejs
mongoose.connect("mongodb://127.0.0.1:27017/MakeMyListDB")
.then(()=>{
    console.log('mongoose connected');
})
.catch((e)=>{
    console.log('Connection failed');
})

//Making listSchema
const listSchema = {
    title :{
        type:String,
        required:true
    },
    items: [String]
};

//Making Schema for user
const userSchema = {
    name:{
        type:String,
        required:true
    },
    email:{
        type: email,
        required:true
    },
    password:{
        type:String,
        required:true
    },
    myList:[listSchema]
};


//Creating model for user
const user= mongoose.model("User",userSchema);


//Creating model for List
const List = mongoose.model("List",listSchema);

export {user,List}