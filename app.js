const express = require("express");
const bodyParser = require("body-parser");
const _=require("lodash");

const {user,List}= require('./mongo')
const app=express();

// set the view engine to ejs
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({extended:true}));
app.use(express.static("public")); //css file was not applied without this
app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));
app.use('/jquery',express.static(__dirname + '/node_modules/jquery/dist/jquery.min.js'));




//Creating three new Items
// const item1 = new Item({name:"Welcome to do List"});
// const item2 = new Item({name:"Hit + to add "});
// const item3 = new Item({name:"<-- Hit to delete"});

// const defaultItems=[item1,item2,item3];



app.get("/",function(req,res){
    
//     Item.find({}).then(function(items){
//         if(items.length===0){
// //Inserting many items in Item model only if it is empty
//           Item.insertMany(defaultItems);
//           res.redirect("/");
//         }else {
//             res.render("list",{Title:"Today",listItems:items});
//         }
//         //mongoose.connection.close()
//     })

res.redirect("/login");   
});

app.get("/login",function(req,res){
    res.render("login");   
    });

    app.get("/register",function(req,res){
        res.render("register");   
        });

// app.get("/:customListName",function(req,res){
// const customListName=_.capitalize(req.params.customListName);

// List.findOne({name:customListName}).then(function(foundList){
//     if(!foundList){
//         //Create a new list
//         const list = new List({
//             name :customListName,
//             items:defaultItems
//         });
//         list.save();
//         res.redirect("/"+customListName);
//     }else{
//         //Show an existing list
//         res.render("list",{Title: foundList.name,listItems:foundList.items});
//     }
// });


// });

// app.post("/",function(req,res){
//     const itemName=req.body.newItem;
//     const listName = req.body.list;

//     const newItem=new Item({name:itemName});

//     if(listName==="Today"){
//         newItem.save();
//         res.redirect("/");
//     }else{
//         List.findOne({name:listName}).then(function(foundList){
//             foundList.items.push(newItem);
//             foundList.save();
//             res.redirect(""+listName);
//         })
//     }
   
// });

// app.post("/delete",function(req,res){
// const checkedItemId = req.body.checkbox;
// const listName=req.body.listName;
//     if(listName==="Today"){
//         Item.findByIdAndRemove(checkedItemId).then(function(){
//             res.redirect("/");
//         })
      
//     }else{
//         List.findOneAndUpdate({name:listName},{$pull:{items:{_id:checkedItemId}}}).then(function(foundList){
//             res.redirect("/"+listName);
//         })
//     }
// });



app.listen(3000,function(){
    console.log("Server started on port 3000");
})